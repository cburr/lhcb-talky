import sys

from flask import Flask, redirect, url_for
from flask_mail import Mail
from flask_wtf.csrf import CSRFProtect

sys.path.insert(0, '/config')
import talky_config

__all__ = ['app', 'mail', 'csrf']

app = Flask(__name__, static_folder='static', static_url_path='')
app.config.from_object(talky_config)
mail = Mail(app)
csrf = CSRFProtect(app)


@app.route('/')
def index():
    return redirect(url_for('security.login'))
